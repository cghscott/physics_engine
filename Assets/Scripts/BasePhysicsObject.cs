using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Physics
{
    public struct KinematicData
    {
        public Vector2 position;
        public Vector2 velocity;
        public Vector2 acceleration;

        public float orientation; // zero is directly to the right
        public float angularVelocity; // positive is anti-clockwise
        public float angularAcceleration;
    }

    public struct Properties
    {
        internal float mass;
        internal float inertia;
        internal float gravityRatio;
        internal float restitution;
        internal float staticFriction;
        internal float dynamicFriction;
        internal float aerialDragCoefficient;
        internal bool isStatic;
    }

    [RequireComponent(typeof(Collider2D))]
    public abstract class BasePhysicsObject : MonoBehaviour
    {
        [SerializeField] protected float _gravityRatio = 1f;
        [SerializeField] protected float magicTorque = 0f;
        [SerializeField] public float mass = 1f;
        [SerializeField] public float inertia = 1f;
        [SerializeField] public float restitution = 1f;
        [SerializeField] public float staticFriction = 1f;
        [SerializeField] public float dynamicFriction = 1f;
        [SerializeField] private bool isStatic = false;
        public KinematicData kinematicData;
        public KinematicData kinematicDataOld;
        public Properties properties;
        public Vector2 desiredPosition { get; protected set; }
        [SerializeField] private Vector2 initialVelocity = Vector2.zero;
        [SerializeField] private float initialAngularVelocity = 0f;

        public new Collider2D collider { get; private set; }

        public Vector2 ComputeForce()
        {
            Vector2 fields = FieldForces();
            Vector2 contacts = ContactForces();
            return contacts + fields;
        }
        protected abstract Vector2 FieldForces();
        protected abstract Vector2 ContactForces();

        public float ComputeTorque()
        {
            float fields = FieldTorques();
            float contacts = ContactTorques();
            return contacts + fields;
        }
        protected abstract float FieldTorques();
        protected abstract float ContactTorques();

        protected virtual void Awake()
        {
            collider = GetComponent<Collider2D>();
            properties.mass = mass;
            properties.inertia = inertia;
            properties.gravityRatio = _gravityRatio;
            properties.restitution = restitution;
            properties.staticFriction = staticFriction;
            properties.dynamicFriction = dynamicFriction;
            properties.isStatic = isStatic;

            kinematicData.position = transform.position;
            kinematicData.velocity = initialVelocity;
            kinematicData.acceleration = Vector2.zero;

            kinematicData.orientation = transform.rotation.eulerAngles.z;
            kinematicData.angularVelocity = initialAngularVelocity;
            kinematicData.angularAcceleration = 0f;
        }
    }
}