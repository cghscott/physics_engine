using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Physics
{
    internal struct MinkowskiVertex
    {
        public Vector2 shape1Vertex { get; private set; }
        public Vector2 shape2Vertex { get; private set; }
        public Vector2 position { get; private set; }

        public MinkowskiVertex(Vector2 shape1Vertex, Vector2 shape2Vertex)
        {
            this.shape1Vertex = shape1Vertex;
            this.shape2Vertex = shape2Vertex;
            position = shape1Vertex - shape2Vertex;
        }
    }

    internal struct SimplexEdge
    {
        public MinkowskiVertex startPoint { get; private set; }
        public MinkowskiVertex endPoint { get; private set; }
        public Vector2 line { get; private set; }
        public Vector2 normal { get; private set; }

        public SimplexEdge(MinkowskiVertex start, MinkowskiVertex end)
        {
            startPoint = start;
            endPoint = end;
            line = end.position - start.position;
            normal = Vector2.Perpendicular(line);
        }

        /// <summary> Returns true if the vector from the endpoint to the origin has a positive component in the direction 
        /// 90 degrees CCW to the vector from the start to the end of the line </summary>
        public bool CCWNormalFacingOrigin()
        {
            return Vector2.Dot(normal, -endPoint.position) > 0f;
        }

        /// <summary> Returns true if a line can be drawn to pass from the edge, orthoginally, to the origin </summary>
        public bool PassesOrigin()
        {
            return Vector2.Dot(-line, -endPoint.position) > 0f;
        }
    }

    internal class SimplexTriangle
    {
        public MinkowskiVertex vertexA { get; private set; }
        public MinkowskiVertex vertexB { get; private set; }
        public MinkowskiVertex vertexC { get; private set; }

        // only need two edges of the triangle as the third (lineAB) has already been checked before the constructer is called
        public SimplexEdge lineBC { get; private set; }
        public SimplexEdge lineCA { get; private set; }

        public SimplexTriangle() { }

        public SimplexTriangle(MinkowskiVertex vertex1, MinkowskiVertex vertex2, MinkowskiVertex vertex3)
        {
            vertexA = vertex1;
            vertexB = vertex2;
            vertexC = vertex3;

            lineBC = new SimplexEdge(vertexB, vertexC);
            lineCA = new SimplexEdge(vertexC, vertexA);
        }

        // Currently not used
        public bool OriginSameDirectionAsBFromC()
        {
            return Vector2.Dot(-lineBC.line, -vertexC.position) > 0;
        }

        public bool OriginSameDirectionAsAFromC()
        {
            return Vector2.Dot(lineCA.line, -vertexC.position) > 0;
        }

        public bool VertexCPastOrigin()
        {
            Vector2 normalAB = Vector2.Perpendicular(vertexB.position - vertexA.position);
            return Vector2.Dot(-normalAB, -vertexC.position) > 0f;
        }
    }

    public static class PhysicsHelper
    {
        private enum SimplexCase
        {
            Collision,
            NoCollision,
            Line,
            Triangle
        }

        public enum ClosestFeaturesState
        {
            Collision,
            TwoPoints,
            PointAndLine,
            TwoLines, // Currently not implemented
            Undefined
        }

        /// <summary> Returns the closest features of two shapes and other necessary information for the collision detection algorithm.</summary>
        public static float ClosestFeatures(List<Vector2> shape1Vertices, List<Vector2> shape2Vertices, Vector2 shape1Centroid, Vector2 shape2Centroid,
            ref bool lineOnShape1, out Vector2 supportAxis, ref Vector2 contactToCentroid1, ref Vector2 contactToCentroid2, 
            ref ClosestFeaturesState featuresCase, ref List<Vector2> line)
        {
            List<Vector2> closestVertices1 = new List<Vector2>();
            List<Vector2> closestVertices2 = new List<Vector2>();

            featuresCase = GJKClosestFeatures(shape1Vertices, shape2Vertices,
                shape1Centroid, shape2Centroid, ref closestVertices1, ref closestVertices2);

            switch (featuresCase)
            {
                case ClosestFeaturesState.Collision:
                    featuresCase = ClosestFeaturesState.PointAndLine;
                    return FindDepthAndSupportAxes(closestVertices1, closestVertices2, out supportAxis, out contactToCentroid1, out contactToCentroid2, 
                        shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid, ref line, out lineOnShape1);

                case ClosestFeaturesState.TwoPoints:
                    supportAxis = (closestVertices2[0] - closestVertices1[0]).normalized;
                    contactToCentroid1 = shape1Centroid - closestVertices1[0];
                    contactToCentroid2 = shape2Centroid - closestVertices2[0];
                    return Vector2.Distance(closestVertices1[0], closestVertices2[0]);

                case ClosestFeaturesState.PointAndLine:
                    return PointAndLineCase(ref lineOnShape1, out supportAxis, out contactToCentroid1, out contactToCentroid2, closestVertices1, closestVertices2, 
                        shape1Centroid, shape2Centroid, ref line);

                case ClosestFeaturesState.TwoLines: // Currently not implemented
                    throw new NotImplementedException();

                default: // Should never get here
                    throw new NotImplementedException();
            }
        }

        /// <summary> Similar to ClosestFeatures but only returns a float representing the distance between the objects. It is negative if the objects are overlapping.</summary>
        public static float ClosestDistancePenetrationDepth(List<Vector2> shape1Vertices, List<Vector2> shape2Vertices, Vector2 shape1Centroid, Vector2 shape2Centroid)
        {
            List<Vector2> closestVertices1 = new List<Vector2>();
            List<Vector2> closestVertices2 = new List<Vector2>();

            ClosestFeaturesState featuresCase = GJKClosestFeatures(shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid,
            ref closestVertices1, ref closestVertices2);

            switch (featuresCase)
            {
                case ClosestFeaturesState.Collision:
                    return FindDepth(closestVertices1, closestVertices2, shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid);

                case ClosestFeaturesState.TwoPoints:
                    return Vector2.Distance(closestVertices1[0], closestVertices2[0]);

                case ClosestFeaturesState.PointAndLine:
                    return PointAndLineCaseDistanceOnly(closestVertices1, closestVertices2);

                case ClosestFeaturesState.TwoLines: // Currently not implemented
                    throw new NotImplementedException();
            }

            return -1f; // should never get here
        }

        /// <summary> Similar to ClosestFeatures but only returns a float representing the distance between the objects. It is zero if the objects are overlapping.</summary>
        public static float ClosestDistanceOnly(List<Vector2> shape1Vertices, List<Vector2> shape2Vertices, Vector2 shape1Centroid, Vector2 shape2Centroid)
        {
            List<Vector2> closestVertices1 = new List<Vector2>();
            List<Vector2> closestVertices2 = new List<Vector2>();

            ClosestFeaturesState featuresCase = GJKClosestFeatures(shape1Vertices, shape2Vertices,
                shape1Centroid, shape2Centroid, ref closestVertices1, ref closestVertices2);

            switch (featuresCase)
            {
                case ClosestFeaturesState.Collision:
                    return 0f;

                case ClosestFeaturesState.TwoPoints:
                    return Vector2.Distance(closestVertices1[0], closestVertices2[0]);

                case ClosestFeaturesState.PointAndLine:
                    return PointAndLineCaseDistanceOnly(closestVertices1, closestVertices2);

                case ClosestFeaturesState.TwoLines: // Currently not implemented
                    throw new NotImplementedException();

                default: // Should never get here
                    throw new NotImplementedException();
            }
        }

        private static float PointAndLineCase(ref bool lineOnShape1, out Vector2 supportAxis, out Vector2 contactToCentroid1, out Vector2 contactToCentroid2,
            List<Vector2> closestVertices1, List<Vector2> closestVertices2, Vector2 shape1Centroid, Vector2 shape2Centroid, ref List<Vector2> line)
        {
            if (closestVertices1[0] == closestVertices1[1])
            {
                lineOnShape1 = false;
                line = closestVertices2;
                supportAxis = Vector2.Perpendicular(line[1] - line[0]).normalized;
                if (Vector2.Dot(supportAxis, closestVertices1[0] - shape1Centroid) < 0)
                    supportAxis = -supportAxis;
                return PointAndLineDistanceAndSupportAxes(out contactToCentroid2, out contactToCentroid1, closestVertices2, closestVertices1[0],
                    shape2Centroid, shape1Centroid);
            }
            else // closestVertices2[0] == closestVertices2[1]
            {
                lineOnShape1 = true;
                line = closestVertices1;
                supportAxis = Vector2.Perpendicular(line[1] - line[0]).normalized;
                if (Vector2.Dot(supportAxis, closestVertices1[0] - shape1Centroid) < 0)
                    supportAxis = -supportAxis;
                return PointAndLineDistanceAndSupportAxes(out contactToCentroid1, out contactToCentroid2, closestVertices1, closestVertices2[0],
                    shape1Centroid, shape2Centroid);
            }
        }
        
        private static float PointAndLineDistanceAndSupportAxes(out Vector2 contactToCentroidLine, out Vector2 contactToCentroidPoint, 
            List<Vector2> closestVerticesLine, Vector2 closestVertexPoint, Vector2 lineCentroid, Vector2 pointCentroid)
        {
            contactToCentroidPoint = pointCentroid - closestVertexPoint;
            Vector2 contactRelativeToStartPoint = Vector2.Dot((closestVerticesLine[1] - closestVerticesLine[0]).normalized, closestVertexPoint - closestVerticesLine[0]) 
                * (closestVerticesLine[1] - closestVerticesLine[0]).normalized;
            contactToCentroidLine = lineCentroid - closestVerticesLine[0] - contactRelativeToStartPoint;
            // Ax + By + C = 0
            // d = |A(x1) + B(y1) + C| / (A^2 + B^2)^0.5
            float A = closestVerticesLine[1].y - closestVerticesLine[0].y;
            float B = closestVerticesLine[0].x - closestVerticesLine[1].x;
            float C = -A * closestVerticesLine[0].x - B * closestVerticesLine[0].y;
            float distance = (A * closestVertexPoint.x + B * closestVertexPoint.y + C) / Mathf.Sqrt(Mathf.Pow(A, 2) + Mathf.Pow(B, 2));

            return Mathf.Abs(distance);
        }

        private static float PointAndLineCaseDistanceOnly(List<Vector2> closestVertices1, List<Vector2> closestVertices2)
        {
            if (closestVertices1[0] == closestVertices1[1])
            {
                return PointAndLineDistanceDistanceOnly(closestVertices2, closestVertices1[0]);
            }
            else // closestVertices2[0] == closestVertices2[1]
            {
                return PointAndLineDistanceDistanceOnly(closestVertices1, closestVertices2[0]);
            }
        }

        public static float PointAndLineDistanceDistanceOnly(List<Vector2> closestVerticesLine, Vector2 closestVerticesPoint)
        {
            // Ax + By + C = 0
            // d = |A(x1) + B(y1) + C| / (A^2 + B^2)^0.5
            float A = closestVerticesLine[1].y - closestVerticesLine[0].y;
            float B = closestVerticesLine[0].x - closestVerticesLine[1].x;
            float C = -A * closestVerticesLine[0].x - B * closestVerticesLine[0].y;
            float distance = (A * closestVerticesPoint.x + B * closestVerticesPoint.y + C) / Mathf.Sqrt(Mathf.Pow(A, 2) + Mathf.Pow(B, 2));

            return Mathf.Abs(distance);
        }

        /// <summary> Similar to ClosestFeatures but only returns a float representing the penetration depth if objects are overlapping. It is zero otherwise.</summary>
        public static float PenetrationDepthOnly(List<Vector2> shape1Vertices, List<Vector2> shape2Vertices, Vector2 shape1Centroid, Vector2 shape2Centroid,
            out Vector2 supportAxis)
        {
            List<Vector2> closestVertices1 = new List<Vector2>();
            List<Vector2> closestVertices2 = new List<Vector2>();
            List<MinkowskiVertex> minkowskiVertices = new List<MinkowskiVertex>();
            List<SimplexEdge> edges = new List<SimplexEdge>();

            ClosestFeaturesState featuresCase = GJKClosestFeatures(shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid,
            ref closestVertices1, ref closestVertices2);

            if (featuresCase == ClosestFeaturesState.Collision)
            {
                return FindDepthAndSupportAxis(closestVertices1, closestVertices2, out supportAxis, shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid);
            }

            supportAxis = Vector2.zero;
            return 0f;
        }

        /// <summary> Similar to PenetrationDepth but also returns the edge of one of the shapes. It is the closest edge (on the shape that doesn't have the deepest penetrating vertex)
        /// to the deepest penetrating vertex.</summary>
        public static float PenetrationDepthAndLine(List<Vector2> shape1Vertices, List<Vector2> shape2Vertices, Vector2 shape1Centroid, Vector2 shape2Centroid,
            out Vector2 supportAxis, ref Vector2 contactToCentroid1, ref Vector2 contactToCentroid2, ref List<Vector2> line, out bool lineOnShape1)
        {
            List<Vector2> closestVertices1 = new List<Vector2>();
            List<Vector2> closestVertices2 = new List<Vector2>();
            List<MinkowskiVertex> minkowskiVertices = new List<MinkowskiVertex>();
            List<SimplexEdge> edges = new List<SimplexEdge>();

            ClosestFeaturesState featuresCase = GJKClosestFeatures(shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid,
            ref closestVertices1, ref closestVertices2);

            if (featuresCase == ClosestFeaturesState.Collision)
            {
                return FindDepthAndSupportAxes(closestVertices1, closestVertices2, out supportAxis, out contactToCentroid1, out contactToCentroid2,
                    shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid, ref line, out lineOnShape1);
            }

            supportAxis = Vector2.zero;
            lineOnShape1 = false;
            return 0f;
        }

        private static float FindDepth(List<Vector2> closestVertices1, List<Vector2> closestVertices2,
            List<Vector2> shape1Vertices, List<Vector2> shape2Vertices, Vector2 shape1Centroid, Vector2 shape2Centroid)
        {
            bool lineOnShape1;
            List<Vector2> line = new List<Vector2>();
            Vector2 supportAxis, contactToCentroid1, contactToCentroid2;
            List<MinkowskiVertex> minkowskiVertices = new List<MinkowskiVertex>();
            List<SimplexEdge> edges = new List<SimplexEdge>();
            InitialisePolytope(ref minkowskiVertices, ref edges, closestVertices1, closestVertices2);
            return EPADepth(ref edges, ref minkowskiVertices, out supportAxis, out contactToCentroid1, out contactToCentroid2, 
                shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid, ref line, out lineOnShape1);
        }

        private static float FindDepthAndSupportAxis(List<Vector2> closestVertices1, List<Vector2> closestVertices2, out Vector2 supportAxis,
            List<Vector2> shape1Vertices, List<Vector2> shape2Vertices, Vector2 shape1Centroid, Vector2 shape2Centroid)
        {
            bool lineOnShape1;
            Vector2 contactToCentroid1;
            Vector2 contactToCentroid2;
            List<Vector2> line = new List<Vector2>();
            List<MinkowskiVertex> minkowskiVertices = new List<MinkowskiVertex>();
            List<SimplexEdge> edges = new List<SimplexEdge>();
            InitialisePolytope(ref minkowskiVertices, ref edges, closestVertices1, closestVertices2);
            return EPADepth(ref edges, ref minkowskiVertices, out supportAxis, out contactToCentroid1, out contactToCentroid2, 
                shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid, ref line, out lineOnShape1);
        }

        private static float FindDepthAndSupportAxes(List<Vector2> closestVertices1, List<Vector2> closestVertices2, out Vector2 supportAxis,
            out Vector2 contactToCentroid1, out Vector2 contactToCentroid2, List<Vector2> shape1Vertices, List<Vector2> shape2Vertices,
            Vector2 shape1Centroid, Vector2 shape2Centroid, ref List<Vector2> line, out bool lineOnShape1)
        {
            List<MinkowskiVertex> minkowskiVertices = new List<MinkowskiVertex>();
            List<SimplexEdge> edges = new List<SimplexEdge>();
            InitialisePolytope(ref minkowskiVertices, ref edges, closestVertices1, closestVertices2);
            return EPADepth(ref edges, ref minkowskiVertices, out supportAxis, out contactToCentroid1, out contactToCentroid2,
                shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid, ref line, out lineOnShape1);
        }

        private static void InitialisePolytope(ref List<MinkowskiVertex> minkowskiVertices, ref List<SimplexEdge> edges,
            List<Vector2> closestVertices1, List<Vector2> closestVertices2)
        {
            MinkowskiVertex minkowskiVertex1 = new MinkowskiVertex(closestVertices1[0], closestVertices2[0]);
            MinkowskiVertex minkowskiVertex2 = new MinkowskiVertex(closestVertices1[1], closestVertices2[1]);
            MinkowskiVertex minkowskiVertex3 = new MinkowskiVertex(closestVertices1[2], closestVertices2[2]);

            minkowskiVertices.Add(minkowskiVertex1);
            minkowskiVertices.Add(minkowskiVertex2);
            minkowskiVertices.Add(minkowskiVertex3);

            // edge 1 to 2 to 3 still running counter clockwise around the simplex so that the normals are facing outwards
            edges.Add(new SimplexEdge(minkowskiVertex1, minkowskiVertex2));
            edges.Add(new SimplexEdge(minkowskiVertex2, minkowskiVertex3));
            edges.Add(new SimplexEdge(minkowskiVertex3, minkowskiVertex1));
        }

        private static float EPADepth(ref List<SimplexEdge> edges, ref List<MinkowskiVertex> minkowskiVertices, out Vector2 supportAxis,
            out Vector2 contactToCentroid1, out Vector2 contactToCentroid2, List<Vector2> shape1Vertices, List<Vector2> shape2Vertices, 
            Vector2 shape1Centroid, Vector2 shape2Centroid, ref List<Vector2> line, out bool lineOnShape1)
        {
            float closestDistance;
            int index = 0;
            while (true)
            {
                closestDistance = MaxClosestDistance(minkowskiVertices);
                closestDistance = CurrentClosestDistance(edges, closestDistance, ref index, out supportAxis, out contactToCentroid1, out contactToCentroid2,
                    shape1Centroid, shape2Centroid, ref line, out lineOnShape1);
                MinkowskiVertex newVertex = FindMinkowskiVertex(shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid, edges[index].normal);
                bool isRepeatedVertex = IsVertexRepeat(minkowskiVertices, newVertex.position);

                if (isRepeatedVertex)
                    return -closestDistance;
                else
                {
                    minkowskiVertices.Add(newVertex);
                    edges.Add(new SimplexEdge(edges[index].startPoint, newVertex));
                    edges.Add(new SimplexEdge(newVertex, edges[index].endPoint));
                    edges.Remove(edges[index]);
                }
            }
        }

        private static float MaxClosestDistance(List<MinkowskiVertex> minkowskiVertices)
        {
            float furthestVertexDistance = 0f;
            foreach (MinkowskiVertex vertex in minkowskiVertices)
            {
                if (vertex.position.magnitude > furthestVertexDistance)
                    furthestVertexDistance = vertex.position.magnitude;
            }
            return furthestVertexDistance;
        }

        private static float CurrentClosestDistance(List<SimplexEdge> edges, float closestDistance, ref int index, out Vector2 supportAxis,
            out Vector2 contactToCentroid1, out Vector2 contactToCentroid2, Vector2 shape1Centroid, Vector2 shape2Centroid, ref List<Vector2> line,
            out bool lineOnShape1)
        {
            for (int i = 0; i < edges.Count; i++)
            {
                SimplexEdge edge = edges[i];
                float A = edge.endPoint.position.y - edge.startPoint.position.y;            // Ax + By + C = 0
                float B = edge.startPoint.position.x - edge.endPoint.position.x;            // d = |A(x1) + B(y1) + C| / (A^2 + B^2)^0.5
                float C = -A * edge.startPoint.position.x - B * edge.startPoint.position.y; // distance from edge to origin so x1 = y1 = 0
                float distance = C / Mathf.Sqrt(Mathf.Pow(A, 2) + Mathf.Pow(B, 2));         // d = C / (A^2 + B^2)^0.5

                if (distance < closestDistance)
                {
                    index = i;
                    line = new List<Vector2>();
                    if (edge.startPoint.shape1Vertex == edge.endPoint.shape1Vertex)
                    {
                        line.Add(edge.startPoint.shape2Vertex);
                        line.Add(edge.endPoint.shape2Vertex);
                    }
                    else
                    {
                        line.Add(edge.startPoint.shape1Vertex);
                        line.Add(edge.endPoint.shape1Vertex);
                    }
                    closestDistance = distance;
                }
            }
            supportAxis = edges[index].normal.normalized;
            if (edges[index].startPoint.shape1Vertex == edges[index].endPoint.shape1Vertex)
                lineOnShape1 = false;
            else // startPoint.shape2Vertex == endPoint.shape2Vertex
                lineOnShape1 = true;

            Vector2 contactRelativeToStartPoint = closestDistance * supportAxis - edges[index].startPoint.position;
            FindContactToEachCentroid(edges[index], out contactToCentroid1, out contactToCentroid2, shape1Centroid, shape2Centroid, contactRelativeToStartPoint);
            return closestDistance;
        }
        
        private static void FindContactToEachCentroid(SimplexEdge edge, out Vector2 contactToCentroid1, out Vector2 contactToCentroid2,
            Vector2 shape1Centroid, Vector2 shape2Centroid, Vector2 contactRelativeToStartPoint)
        {
            if (edge.startPoint.shape1Vertex == edge.endPoint.shape1Vertex)
            {
                contactToCentroid1 = shape1Centroid - edge.startPoint.shape1Vertex;
                contactToCentroid2 = shape2Centroid - edge.startPoint.shape2Vertex + contactRelativeToStartPoint;
            }
            else // startPoint.shape2Vertex == endPoint.shape2Vertex
            {
                contactToCentroid1 = shape1Centroid - edge.startPoint.shape1Vertex - contactRelativeToStartPoint;
                contactToCentroid2 = shape2Centroid - edge.startPoint.shape2Vertex;
            }
        }

        private static bool IsVertexRepeat(List<MinkowskiVertex> minkowskiVertices, Vector2 newVertexPosition)
        {
            bool isRepeatedVertex = false;
            foreach (MinkowskiVertex vertex in minkowskiVertices)
            {
                if (newVertexPosition == vertex.position)
                    isRepeatedVertex = true;
            }
            return isRepeatedVertex;
        }

        // GJK algorithm start
        public static ClosestFeaturesState GJKClosestFeatures(List<Vector2> shape1Vertices, List<Vector2> shape2Vertices, 
            Vector2 shape1Centroid, Vector2 shape2Centroid, ref List<Vector2> closestVertices1, ref List<Vector2> closestVertices2)
        {
            Vector2 pilotDirection = shape2Centroid - shape1Centroid;
            MinkowskiVertex minkowskiVertex1 = FindMinkowskiVertex(shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid, pilotDirection);
            MinkowskiVertex minkowskiVertex2 = FindMinkowskiVertex(shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid, -minkowskiVertex1.position);
            SimplexEdge line = new SimplexEdge(minkowskiVertex1, minkowskiVertex2);

            SimplexCase simplexCase = SimplexCase.NoCollision;
            ClosestFeaturesState featuresCase = TryInitializeSimplexLine(ref minkowskiVertex1, ref minkowskiVertex2, ref closestVertices1, ref closestVertices2,
                ref line, ref simplexCase, shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid);

            if (simplexCase != SimplexCase.Line)
                return featuresCase;

            SimplexTriangle simplex = new SimplexTriangle();
            LineCaseClosestFeatures(ref line, ref simplex, ref simplexCase, ref featuresCase, ref closestVertices1, ref closestVertices2,
                shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid);

            if (simplexCase == SimplexCase.Triangle)
            {
                TriangleCaseClosestFeatures(ref simplex, ref simplexCase, ref featuresCase, ref closestVertices1, ref closestVertices2,
                    shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid);
            }

            if (simplexCase == SimplexCase.Collision)
                return ClosestFeaturesState.Collision;

            return featuresCase;
        }

        private static MinkowskiVertex FindMinkowskiVertex(List<Vector2> shape1Vertices, List<Vector2> shape2Vertices,
            Vector2 shape1Centroid, Vector2 shape2Centroid, Vector2 direction)
        {
            Vector2 shape1Vertex = FurthestPoint(shape1Vertices, direction, shape1Centroid);
            Vector2 shape2Vertex = FurthestPoint(shape2Vertices, -direction, shape2Centroid);
            MinkowskiVertex minkowskiVertex = new MinkowskiVertex(shape1Vertex, shape2Vertex);
            return minkowskiVertex;
        }

        /// <summary> Returns the shape's furthest vertex from the centroid in the given direction </summary>
        public static Vector2 FurthestPoint(List<Vector2> shapeVertices, Vector2 direction, Vector2 shapeCentroid)
        {
            Vector2 furthestPoint = shapeVertices[0];
            float furthestDistance = 0f;
            foreach (var vertex in shapeVertices)
            {
                Vector2 vertexRelative = vertex - shapeCentroid; // Has to be the furthest point in the given direction from the shape centroid
                float distance = Vector2.Dot(vertexRelative, direction);
                if (distance > furthestDistance)
                {
                    furthestDistance = distance;
                    furthestPoint = vertex;
                }
            }

            return furthestPoint;
        }

        public static List<Vector2> TryFindEquidistantPoints(List<Vector2> shapeVertices, Vector2 direction, Vector2 shapeCentroid, float targetDistance, 
            float tolerance, out bool verticesFound)
        {
            verticesFound = false;
            List<Vector2> result = new List<Vector2>();
            foreach (var vertex in shapeVertices)
            {
                Vector2 vertexRelative = vertex - shapeCentroid; // Has to be the furthest point in the given direction from the shape centroid
                float distance = Vector2.Dot(vertexRelative, direction);
                if (distance <= targetDistance + tolerance && distance >= targetDistance - tolerance) 
                    result.Add(vertex);
            }
            if (result.Count >= 2) 
                verticesFound = true;
            return result;
        }

        private static ClosestFeaturesState TryInitializeSimplexLine(ref MinkowskiVertex minkowskiVertex1, ref MinkowskiVertex minkowskiVertex2,
            ref List<Vector2> closestVertices1, ref List<Vector2> closestVertices2, ref SimplexEdge line, ref SimplexCase simplexCase,
            List<Vector2> shape1Vertices, List<Vector2> shape2Vertices, Vector2 shape1Centroid, Vector2 shape2Centroid)
        {
            while (true)
            {
                if (minkowskiVertex1.position == minkowskiVertex2.position) // no collision
                {
                    closestVertices1.Add(minkowskiVertex1.shape1Vertex);
                    closestVertices2.Add(minkowskiVertex1.shape2Vertex);
                    return ClosestFeaturesState.TwoPoints;
                }
                else if (line.PassesOrigin()) // Line Case AB
                {
                    simplexCase = SimplexCase.Line;
                    break;
                }
                else // B is new A, search for new B
                {
                    minkowskiVertex1 = minkowskiVertex2;
                    minkowskiVertex2 = FindMinkowskiVertex(shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid, -minkowskiVertex1.position);
                    line = new SimplexEdge(minkowskiVertex1, minkowskiVertex2);
                }
            }
            return ClosestFeaturesState.Undefined;
        }

        private static void LineCaseClosestFeatures(ref SimplexEdge line, ref SimplexTriangle simplex, ref SimplexCase simplexCase, ref ClosestFeaturesState featuresCase,
            ref List<Vector2> closestVertices1, ref List<Vector2> closestVertices2, 
            List<Vector2> shape1Vertices, List<Vector2> shape2Vertices, Vector2 shape1Centroid, Vector2 shape2Centroid)
        {
            while (simplexCase == SimplexCase.Line)
            {
                if (line.startPoint.position == line.endPoint.position)
                {
                    closestVertices1.Add(line.startPoint.shape1Vertex);
                    closestVertices2.Add(line.startPoint.shape2Vertex);
                    simplexCase = SimplexCase.NoCollision;
                    featuresCase = ClosestFeaturesState.TwoPoints;
                    break;
                }

                if (line.CCWNormalFacingOrigin()) // Checking which side of the line faces the origin
                {
                    LineCaseTryFindNewVertex(ref line, ref simplex, ref simplexCase, ref featuresCase, ref closestVertices1, ref closestVertices2,
                        1f, shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid);
                }
                else
                {
                    LineCaseTryFindNewVertex(ref line, ref simplex, ref simplexCase, ref featuresCase, ref closestVertices1, ref closestVertices2,
                        -1f, shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid);
                }
            }
        }

        private static void LineCaseTryFindNewVertex(ref SimplexEdge line, ref SimplexTriangle simplex, ref SimplexCase simplexCase, ref ClosestFeaturesState featuresCase,
            ref List<Vector2> closestVertices1, ref List<Vector2> closestVertices2, float direction,
            List<Vector2> shape1Vertices, List<Vector2> shape2Vertices, Vector2 shape1Centroid, Vector2 shape2Centroid)
        {
            MinkowskiVertex minkowskiVertex3 = FindMinkowskiVertex(shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid, direction * line.normal); // new vertex C

            if (minkowskiVertex3.position == line.startPoint.position || minkowskiVertex3.position == line.endPoint.position)
            {
                closestVertices1.Add(line.startPoint.shape1Vertex); // the two Minkowski Vertices on the line must share a vertex on one of the colliders
                closestVertices1.Add(line.endPoint.shape1Vertex);
                closestVertices2.Add(line.startPoint.shape2Vertex);
                closestVertices2.Add(line.endPoint.shape2Vertex);
                simplexCase = SimplexCase.NoCollision;
                featuresCase = ClosestFeaturesState.PointAndLine; // shared vertex and the line between the two different vertices on the other collider
                return;
            }

            if (Vector2.Dot(minkowskiVertex3.position, direction * line.normal) > 0)
            {
                simplexCase = SimplexCase.Triangle;
                simplex = InitialSimplexTriangle(line, minkowskiVertex3, direction);
            }
            else
            {
                TryFindNewLine(ref line, direction, minkowskiVertex3, shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid);
            }
        }

        private static SimplexTriangle InitialSimplexTriangle(SimplexEdge line, MinkowskiVertex minkowskiVertex3, float direction)
        {
            if (direction == 1f) // Minkowski vertex 1, 2, 3 as A, B, C (new vertex must be last for the constructor to work)
                return new SimplexTriangle(line.startPoint, line.endPoint, minkowskiVertex3);
            else // 2, 1, 3 as A, B, C
                return new SimplexTriangle(line.endPoint, line.startPoint, minkowskiVertex3);
        }

        private static void TryFindNewLine(ref SimplexEdge line, float direction, MinkowskiVertex minkowskiVertex3,
            List<Vector2> shape1Vertices, List<Vector2> shape2Vertices, Vector2 shape1Centroid, Vector2 shape2Centroid)
        {
            SimplexEdge lineAC;
            bool ACGoesPastOrigin;
            SimplexEdge lineCB;
            bool CBGoesPastOrigin;
            if (direction == 1f) // A = startPoint, B = endPoint
            {
                lineAC = new SimplexEdge(line.startPoint, minkowskiVertex3);
                ACGoesPastOrigin = lineAC.PassesOrigin();
                lineCB = new SimplexEdge(minkowskiVertex3, line.endPoint);
                CBGoesPastOrigin = lineCB.PassesOrigin();
            }
            else // B = startPoint, A = endPoint
            {
                lineAC = new SimplexEdge(line.endPoint, minkowskiVertex3);
                ACGoesPastOrigin = lineAC.PassesOrigin();
                lineCB = new SimplexEdge(minkowskiVertex3, line.startPoint);
                CBGoesPastOrigin = lineCB.PassesOrigin();
            }

            if (ACGoesPastOrigin && CBGoesPastOrigin)
            {
                if (lineAC.CCWNormalFacingOrigin())
                    line = lineAC;
                else
                    line = lineCB;
            }
            else if (!ACGoesPastOrigin && !CBGoesPastOrigin)
            {
                MinkowskiVertex newVertexA = line.startPoint;
                if (lineAC.CCWNormalFacingOrigin())
                    newVertexA = FindMinkowskiVertex(shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid, lineAC.normal);   

                MinkowskiVertex newVertexB = line.endPoint;
                if (lineCB.CCWNormalFacingOrigin())
                    newVertexB = FindMinkowskiVertex(shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid, lineCB.normal);

                if ((newVertexA.position != line.startPoint.position && newVertexA.position != minkowskiVertex3.position)
                    || (newVertexB.position != line.endPoint.position && newVertexB.position != minkowskiVertex3.position))
                {
                    line = new SimplexEdge(newVertexA, newVertexB);
                }
                else
                    line = new SimplexEdge(minkowskiVertex3, minkowskiVertex3);
            }
            else if (ACGoesPastOrigin)
                line = lineAC;
            else
                line = lineCB;
        }

        private static void TriangleCaseClosestFeatures(ref SimplexTriangle simplex, ref SimplexCase simplexCase, ref ClosestFeaturesState featuresCase,
            ref List<Vector2> closestVertices1, ref List<Vector2> closestVertices2,
            List<Vector2> shape1Vertices, List<Vector2> shape2Vertices, Vector2 shape1Centroid, Vector2 shape2Centroid)
        {
            while (simplexCase == SimplexCase.Triangle)
            {
                if (simplex.lineBC.CCWNormalFacingOrigin())
                {
                    TriangleCaseTryFindNewVertex(ref simplex, ref simplexCase, ref featuresCase, ref closestVertices1, ref closestVertices2,
                        simplex.vertexB, simplex.vertexC, simplex.lineBC, shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid);
                }
                else if (simplex.lineCA.CCWNormalFacingOrigin())
                {
                    TriangleCaseTryFindNewVertex(ref simplex, ref simplexCase, ref featuresCase, ref closestVertices1, ref closestVertices2,
                        simplex.vertexC, simplex.vertexA, simplex.lineCA, shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid);
                }
                else
                {
                    closestVertices1.Add(simplex.vertexA.shape1Vertex);
                    closestVertices1.Add(simplex.vertexB.shape1Vertex);
                    closestVertices1.Add(simplex.vertexC.shape1Vertex);
                    closestVertices2.Add(simplex.vertexA.shape2Vertex);
                    closestVertices2.Add(simplex.vertexB.shape2Vertex);
                    closestVertices2.Add(simplex.vertexC.shape2Vertex);
                    simplexCase = SimplexCase.Collision;
                    featuresCase = ClosestFeaturesState.Collision;
                    break;
                }
            }
        }

        private static void TriangleCaseTryFindNewVertex(ref SimplexTriangle simplex, ref SimplexCase simplexCase, ref ClosestFeaturesState featuresCase,
            ref List<Vector2> closestVertices1, ref List<Vector2> closestVertices2, MinkowskiVertex vertex1, MinkowskiVertex vertex2, SimplexEdge line,
            List<Vector2> shape1Vertices, List<Vector2> shape2Vertices, Vector2 shape1Centroid, Vector2 shape2Centroid)
        {
            MinkowskiVertex newVertex = FindMinkowskiVertex(shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid, line.normal);

            if (newVertex.position == vertex1.position || newVertex.position == vertex2.position) // 1 and 2 should have a common vertex
            {
                closestVertices1.Add(vertex1.shape1Vertex);
                closestVertices1.Add(vertex2.shape1Vertex);
                closestVertices2.Add(vertex1.shape2Vertex);
                closestVertices2.Add(vertex2.shape2Vertex);
                simplexCase = SimplexCase.NoCollision;
                featuresCase = ClosestFeaturesState.PointAndLine; // the point defined by the common vertex and the line between the two different vertices on the other shape
            }
            else
            {
                simplex = new SimplexTriangle(vertex2, vertex1, newVertex);
            }
        }


        /// <summary> Faster than GJKClosestFeatures but only returns a boolean. Not currently used.</summary>
        public static bool GJKBool(List<Vector2> shape1Vertices, List<Vector2> shape2Vertices, Vector2 shape1Centroid, Vector2 shape2Centroid)
        {
            Vector2 pilotDirection = shape2Centroid - shape1Centroid;

            MinkowskiVertex minkowskiVertex1 = FindMinkowskiVertex(shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid, pilotDirection);
            MinkowskiVertex minkowskiVertex2 = FindMinkowskiVertex(shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid, -minkowskiVertex1.position);

            SimplexEdge lineAB = new SimplexEdge(minkowskiVertex1, minkowskiVertex2);
            SimplexCase simplexCase = SimplexCase.NoCollision;

            // Validating Line Case
            while (simplexCase == SimplexCase.NoCollision)
            {
                if (minkowskiVertex1.position == minkowskiVertex2.position)
                {
                    // no collision
                    return false;
                }

                if (lineAB.PassesOrigin())
                {
                    // Line Case AB
                    simplexCase = SimplexCase.Line;
                    break;
                }
                else
                {
                    // B is new A, search for new B
                    minkowskiVertex1 = minkowskiVertex2;
                    minkowskiVertex2 = FindMinkowskiVertex(shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid, -minkowskiVertex1.position);
                }
            }

            SimplexTriangle simplex = new SimplexTriangle();

            while(simplexCase != SimplexCase.NoCollision)
            {
                LineCase(ref lineAB, ref simplex, ref simplexCase, shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid);

                if (simplexCase == SimplexCase.Triangle)
                {
                    TriangleCase(ref simplex, ref simplexCase, shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid);
                }

                if (simplexCase == SimplexCase.Collision)
                {
                    return true;
                }
            }

            return false;
        }

        private static void LineCase(ref SimplexEdge line, ref SimplexTriangle simplex, ref SimplexCase simplexCase,
            List<Vector2> shape1Vertices, List<Vector2> shape2Vertices, Vector2 shape1Centroid, Vector2 shape2Centroid)
        {
            while (simplexCase == SimplexCase.Line)
            {
                if (line.startPoint.position == line.endPoint.position)
                {
                    simplexCase = SimplexCase.NoCollision;
                    break;
                }

                // Checking which side of the line faces the origin
                if (line.CCWNormalFacingOrigin())
                {
                    MinkowskiVertex minkowskiVertex3 = FindMinkowskiVertex(shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid, line.normal);

                    if (minkowskiVertex3.position == line.startPoint.position || minkowskiVertex3.position == line.endPoint.position)
                    {
                        simplexCase = SimplexCase.NoCollision;
                        break;
                    }

                    if (Vector2.Dot(-minkowskiVertex3.position, -line.normal) > 0)
                    {
                        // if origin outside line, 1 to 2 to 3 as A, B, C (new vertex must be last)
                        simplex = new SimplexTriangle(line.startPoint, line.endPoint, minkowskiVertex3);
                        simplexCase = SimplexCase.Triangle;
                    }
                    else
                    {
                        // new line case BC
                        line = new SimplexEdge(line.endPoint, minkowskiVertex3);
                    }
                }
                else
                {
                    MinkowskiVertex minkowskiVertex3 = FindMinkowskiVertex(shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid, -line.normal);

                    if (minkowskiVertex3.position == line.startPoint.position || minkowskiVertex3.position == line.endPoint.position)
                    {
                        simplexCase = SimplexCase.NoCollision;
                        break;
                    }

                    if (Vector2.Dot(-minkowskiVertex3.position, line.normal) > 0)
                    {
                        // if origin inside, 2 to 1 to 3
                        simplex = new SimplexTriangle(line.endPoint, line.startPoint, minkowskiVertex3);
                        simplexCase = SimplexCase.Triangle;
                    }
                    else
                    {
                        // new line case BC
                        line = new SimplexEdge(line.endPoint, minkowskiVertex3);
                    }
                }
            }
        }

        private static void TriangleCase(ref SimplexTriangle simplex, ref SimplexCase simplexCase, List<Vector2> shape1Vertices, 
            List<Vector2> shape2Vertices, Vector2 shape1Centroid, Vector2 shape2Centroid)
        {
            while (simplexCase == SimplexCase.Triangle)
            {
                if (simplex.lineBC.CCWNormalFacingOrigin())
                {
                    MinkowskiVertex newVertex = FindMinkowskiVertex(shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid, simplex.lineBC.normal);

                    if (newVertex.position == simplex.vertexB.position || newVertex.position == simplex.vertexC.position)
                    {
                        simplexCase = SimplexCase.NoCollision;
                        break;
                    }
                    else
                    {
                        simplex = new SimplexTriangle(simplex.vertexC, simplex.vertexB, newVertex);
                    }
                }
                else if (simplex.lineCA.CCWNormalFacingOrigin())
                {
                    MinkowskiVertex newVertex = FindMinkowskiVertex(shape1Vertices, shape2Vertices, shape1Centroid, shape2Centroid, simplex.lineCA.normal);

                    if (newVertex.position == simplex.vertexA.position || newVertex.position == simplex.vertexC.position)
                    {
                        simplexCase = SimplexCase.NoCollision;
                        break;
                    }
                    else
                    {
                        simplex = new SimplexTriangle(simplex.vertexA, simplex.vertexC, newVertex);
                    }
                }
                else
                {
                    simplexCase = SimplexCase.Collision;
                    break;
                }
            }
        }
    }
}
