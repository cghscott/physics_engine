using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Physics
{
    public class PhysicsManager : MonoBehaviour
    {
        [SerializeField] private List<RigidBodyMotion> _dynamicObjects;
        [SerializeField] private List<BasePhysicsObject> _staticObjects;
        [SerializeField] private List<RigidBodyMotion> _rigidBodyMotions;
        [SerializeField] private float _tolerance = 0.0005f;
        [SerializeField] public Vector2 globalGravity;
        [SerializeField] private new CameraFollow camera;

        private List<int> rigidBodyIndex1;
        private List<int> rigidBodyIndex2;
        private RigidBodyMotion _rigidBodyI;
        private RigidBodyMotion _rigidBodyJ;

        private void FixedUpdate()
        {
            ClearCollisionsAll();
            UpdateAccelerations();
            ResolveCollisions();
            UpdateTransforms();
            camera.SetPosition();
        }

        public void AddRigidBodyMotion(RigidBodyMotion rigidBodyMotion)
        {
            _rigidBodyMotions.Add(rigidBodyMotion);
            if (rigidBodyMotion.properties.isStatic)
                _staticObjects.Add(rigidBodyMotion);
            else
                _dynamicObjects.Add(rigidBodyMotion);
        }

        private void ClearCollisionsAll()
        {
            foreach (var rigidBodyMotion in _rigidBodyMotions)
            {
                rigidBodyMotion.ClearCollisions();
            }
        }

        private void UpdateAccelerations()
        {
            foreach (var physicsObj in _dynamicObjects)
            {
                physicsObj.UpdateAcceleration();
            }
        }

        private void ResolveCollisions()
        {
            Collisions.BroadPhaseDetection(_rigidBodyMotions, out rigidBodyIndex1, out rigidBodyIndex2);
            for (int k = 0; k < rigidBodyIndex1.Count; k++)
            {
                int i = rigidBodyIndex1[k];
                int j = rigidBodyIndex2[k];
                _rigidBodyI = _rigidBodyMotions[i];
                _rigidBodyJ = _rigidBodyMotions[j];
                float collisionTime = Collisions.CollisionTime(ref _rigidBodyI, ref _rigidBodyJ, _tolerance);
                   _rigidBodyMotions[i] = _rigidBodyI;
                _rigidBodyMotions[j] = _rigidBodyJ;
            }
            for (int k = 0; k < rigidBodyIndex1.Count; k++)
            {
                int i = rigidBodyIndex1[k];
                int j = rigidBodyIndex2[k];
                _rigidBodyI = _rigidBodyMotions[i];
                _rigidBodyJ = _rigidBodyMotions[j];
                Collisions.EndPositionCorrection(ref _rigidBodyI, ref _rigidBodyJ, _tolerance);
            }
        }

        private void UpdateTransforms()
        {
            foreach (var physicsObj in _dynamicObjects)
            {
                physicsObj.UpdateTransform();
            }
        }
    }
}
