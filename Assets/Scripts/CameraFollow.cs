using Physics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private BasePhysicsObject target;

    public void SetPosition()
    {
        transform.position = target.transform.position + new Vector3(0, 0, -10);
    }
}
