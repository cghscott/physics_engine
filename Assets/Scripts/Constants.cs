using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Physics
{
    public static class Constants
    {
        public static Vector2 Gravity()
        {
            return Vector2.down * 9.8f;
        }
    }
}
