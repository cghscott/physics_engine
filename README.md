<!DOCTYPE html><html><head><meta charset="utf-8"><title>Physics Engine.md</title><style></style></head><body id="preview">
<h1 class="code-line" data-line-start=0 data-line-end=1><a id="Physics_Engine_0"></a>Physics Engine</h1>
<p class="has-line-data" data-line-start="2" data-line-end="3">This Physics engine is inspired by Box2D. It is written in C# for use with Unity and comes contained within a project that should open up in Unity without any extra work. It makes use of the Unity vector libraries and colliders but it has a distinct class called "RigidBodyMotion" that is used as a component on a game object in place of a Unity Rigidbody because it needed extra parameters that aren't available in the standard Unity Rigidbody. It was built in the editor version 2021.3.15f1 but it shouldn't have any issues with later versions of Unity as it doesn't heavily depend on the Unity API.</p>
<p class="has-line-data" data-line-start="3" data-line-end="4">Like Box2D, it features continuous collision detection which makes use of a combination of a modified GJK algorithm and the EPA (Expanding Polytope Algorithm). The collision response, which includes rotation and friction, is fundamentally impulse based but it calculates a duration for each collision based on the component of velocity at a normal to the collision and the tolerance in distance for the collision which allows for finite forces and torques to be used for the final position calculations. Friction is therefore also applied gradually as an acceleration and angular acceleration in this way.</p>
<p class="has-line-data" data-line-start="3" data-line-end="4">After all the positions of the objects have been calculated for the next frame, there is an extra position correction step that eliminates any final overlap in the objects. Ideally this shouldn't be necessary but it deals with any initial overlap between objects as they spawn in and it helps deal with any mistakes the engine makes. It reduces the significance of a known problem with collisions resulting in a lesser issue described below.</p>
<h2 class="code-line" data-line-start=4 data-line-end=5><a id="Installation_4"></a>Installation</h2>
<p class="has-line-data" data-line-start="6" data-line-end="9">Make sure you have Unity installed.<br>
Download the Zip or clone the HTTPS link and use Git Bash to download it.<br>
Make sure the demo project is in the correct folder and open it in Unity.</p>
<h2 class="code-line" data-line-start=10 data-line-end=11><a id="Usage_10"></a>Usage</h2>
<p class="has-line-data" data-line-start="12" data-line-end="13">There is a “Physics Manager” already in the project. You need this. It is an object with the 'PhysicsManager' component. That component is the main physics script which automatically keeps lists of all the Objects with a RigidBodyMotion, handles the broad phase collision detection and calls functions on the objects in the lists. It has the following parameters:</p>
<ul>
<li class="has-line-data" data-line-start="13" data-line-end="15">
<p class="has-line-data" data-line-start="13" data-line-end="14">Tolerance (float) - the distance within which two objects must be to count as colliding at that moment. If they are too far apart, they have not yet collided, if they are overlapping by too much, an earlier time must be found for the collision.</p>
</li>
<li class="has-line-data" data-line-start="15" data-line-end="16">
<p class="has-line-data" data-line-start="15" data-line-end="16">Global Gravity (Unity.Vector2) - sets a vector to be the base acceleration due to gravity for all objects. It can be in any direction.</p>
</li>
<li class="has-line-data" data-line-start="16" data-line-end="18">
<p class="has-line-data" data-line-start="16" data-line-end="17">Camera (Unity object) - tells it which camera to move. The camera movement is very basic and was added to make testing the physics slightly easier.</p>
</li>
</ul>
<p class="has-line-data" data-line-start="18" data-line-end="20">Add a RigidBodyMotion script as a component to any object upon which you desire to impose the laws of physics. It is used instead of the Unity Rigidbody.<br>
The component has the following parameters:</p>
<ul>
<li class="has-line-data" data-line-start="20" data-line-end="22">
<p class="has-line-data" data-line-start="20" data-line-end="21">Gravity Ratio (float) - multiplies the global gravity by this value before applying it to this object.</p>
</li>
<li class="has-line-data" data-line-start="22" data-line-end="23">
<p class="has-line-data" data-line-start="22" data-line-end="23">Magic Torque (float) - applies a constant torque to the object at the value you set. Positive is counter clockwise.</p>
</li>
<li class="has-line-data" data-line-start="23" data-line-end="24">
<p class="has-line-data" data-line-start="23" data-line-end="24">Mass (float) - sets the mass of the object.</p>
</li>
<li class="has-line-data" data-line-start="24" data-line-end="25">
<p class="has-line-data" data-line-start="24" data-line-end="25">Inertia (float) sets the moment of inertia for the object (which affects rotation similarly to how mass affects linear motion).</p>
</li>
<li class="has-line-data" data-line-start="25" data-line-end="26">
<p class="has-line-data" data-line-start="25" data-line-end="26">Restitution (float) - this value is multiplied by the restitution of any object it is colliding with in order to determine the coefficient of restitution for the collision (the ratio between the relative incident velocity and the relative bounce velocity (speed before and after)).</p>
</li>
<li class="has-line-data" data-line-start="26" data-line-end="27">
<p class="has-line-data" data-line-start="26" data-line-end="27">Static Friction (float) - multiplied by the static friction of any other object this one is in contact with to determine coefficient of static friction. This is used to determine the amount of force required to overcome friction and begin moving from rest (becomes meaningless if set to a lower value than Dynamic Friction).</p>
</li>
<li class="has-line-data" data-line-start="27" data-line-end="28">
<p class="has-line-data" data-line-start="27" data-line-end="28">Dynamic Friction (float) - multiplied by the dynamic friction of any other objects this one is in contact with to determine the friction coefficient when in motion or at the moment where static friction is overcome.</p>
</li>
<li class="has-line-data" data-line-start="28" data-line-end="29">
<p class="has-line-data" data-line-start="28" data-line-end="29">Is Static (bool) - prevents the object from moving or colliding with other static objects. Collisions between static and dynamic objects use different calculations as the static objects can be treated as if they have infinite mass which actually simplifies the equations.</p>
</li>
<li class="has-line-data" data-line-start="29" data-line-end="30">
<p class="has-line-data" data-line-start="29" data-line-end="30">Initial Velocity (Unity.Vector2) - sets a vector to be the velocity at which object is already moving at the start.</p>
</li>
<li class="has-line-data" data-line-start="30" data-line-end="31">
<p class="has-line-data" data-line-start="30" data-line-end="31">Initial Angular Velocity (float) - sets the angular velocity of the object at the start (positive is counter clockwise).</p>
</li>
<li class="has-line-data" data-line-start="31" data-line-end="33">
<p class="has-line-data" data-line-start="31" data-line-end="32">Physics Manager (Unity object) - tells it where to look for the Physics Manager.</p>
</li>
</ul>
<p class="has-line-data" data-line-start="33" data-line-end="34">Any unmentioned parameters are read-only for debugging.</p>
<h2 class="code-line" data-line-start=35 data-line-end=36><a id="Known_Issues_35"></a>Known Issues</h2>
<p class="has-line-data" data-line-start="37" data-line-end="38">Due to the fact that the collisions are taken at a single point and can’t overlap in time, there is a slight feedback loop where rotation in collisions (particularly due to friction) can lead to a slight overlap of shapes at the corners which gets position corrected later on so that a single point is touching and the object is allowed to rotate the other way. If there was no position correction, the issue would instead lead to the objects gradually tunneling into eachother. The fix should be quite simple but long winded and it involves some structural changes. Since I plan to rewrite the entire engine in a way that can be used without Unity it may not be fixed in this iteration.</p>
<p class="has-line-data" data-line-start="39" data-line-end="40">The broad phase collision detection is quite simple so it won't be very performant for large numbers of objects. This will also be improved on in future iterations.</p>
<p class="has-line-data" data-line-start="41" data-line-end="42">Since it makes use of the GJK algorithm to detect collisions, it can't accurately detect concave shapes. It instead skips out any concave vertices, treating it as a straight line between the vertices to either side. This is normally solved by treating concave polygons as composites of convex polygons but this engine doesn't currently have a procedure to do this or a means to join two polygons together manually.</p>
</body></html>
